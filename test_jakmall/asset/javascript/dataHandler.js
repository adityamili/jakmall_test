function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function loadItem(data){
	$("#tab-item-holder").html("");
	for(var i=0;i<data.length;i++){

		var promo="";
		if(data[i].promo){
			promo="<div class='promo-info'>"+
					"<article>Best Price</article>"+
				"</div>";
		}
		var btn_text=(data[i].stock>0)?"Buy Now":"Out of Stock";
		var btn_class=(data[i].stock>0)?"available":"not-available";

		var item="<div class='product-item'>"+promo+
					"<div class='image-item'><img src='"+data[i].image+"'></div>"+
					"<div class='item-detail-holder'>"+
						"<div class='item-detail name'>"+
						"<h4>"+data[i].name+"</h4>"+
					"</div>"+
					"<div class='item-detail price'>"+
						"<article>Rp. "+formatNumber(data[i].price)+"</article>"+
					"</div>"+
					"<div class='item-detail buyMe "+btn_class+"'>"+
						"<button>"+btn_text+"</button>"+
					"</div>"+
				"</div>";

		$("#tab-item-holder").append(item);

	}
}

function loadTab(elem,coll){
	$(".tab-head-item").removeClass("active");
	elem.addClass("active");
	loadItem(coll);
}

$("#product-list").on('click',".electronic:not(.active)",function(){
	loadTab($(this),electronic);
});

$("#product-list").on('click',".fashion:not(.active)",function(){
	loadTab($(this),fashion);
});

$("#product-list").on('click',".others:not(.active)",function(){
	loadTab($(this),others);
});

loadItem(electronic);